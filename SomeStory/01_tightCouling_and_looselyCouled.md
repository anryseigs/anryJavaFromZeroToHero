# Khái niệm tight-coupling (liên kết rằng buộc) và cách loosely coupled

- Source [@Loda](https://loda.me/khai-niem-tight-coupling-lien-ket-rang-buoc-va-cach-loosely-coupled-loda1557323622585/)

## Giới thiệu

Trong strong type code, `tight-coupling` ám chỉ việc liên kết giữa các class quá chặt chẽ. Làm khó trong việc
thay đổi cũng như linh động nghiệp vụ, khi mà thay đổi 1 class có thể dẫn đến ảnh hưởng tới 1 loạt các class 
khác có liên kết.

Để hạn chế tinh liên kết cứng như vậy, developer thiết kế code để làm giảm tính lệ thuộc giữa các class,
logic không bị bó buộc giữa các class với nhau => cách thức `loosely coupled`

## Ví dụ code

Ví dụ bạn có 1 nghiệp vụ phức tạp, bao hàm nhiều các nghiệp vụ khác nhau gộp lại. 

### Cách code LV1

```java
public class BubbleSortAlg {
    public void sort(int[] array) {
        // some code in here
        System.out.println("Đã sắp xếp thep thuật toán nổi bọt");
    }
}

public class VeryComplexService {
    private BubbleSortAlg bubbleSortAlg = new BubbleSortAlg();
    
    public VeryComplexService() {}
    
    public void complexBusiness(int[] array){
        bubbleSortAlg.sort(array);
    }
}
```

Với cách code trên, `VeryComplexService` đã hoàn thành được nghiệp vụ của mình.
Tuy nhiên khi business model thay đổi, thuật toàn `BubbleSortAlg` bị đổi sang dạng khác là `QuickSort`,
thì developer phải sửa cả 2 class trên.

Ngoài ra `BubbleSortAlg` chỉ tồn tại khi `VeryComplexService` tồn tại do cách implement này
=> 2 class liên kết chặt chẽ với nhau.

### Cách code LV2

```java
public interface SortAlg {
    void sort(int[] array);
}

public class BubbleSortAlg implements SortAlg{
    @Override 
    public void sort(int[] array) {
        // code bubble sort here
        System.out.println("Đã sắp xếp thep thuật toán nổi bọt");
    }
}

public class VeryComplexService {
    private SortAlg sortAlg;
    public VeryComplexService() {
        sortAlg = new BubbleSortAlg();
    }
    
    public void complexBusiness(int[] array) {
        sortAlg.sort(array);
    }
}
```

Với cách làm này thì `VeryComplexService` chỉ quan hệ với interface `SortAlg`, nhưng nó vẫn giữ liên kết với `BubbleSortAlgorithm`.

=> cách làm này làm giảm lệ thuộc, nhưng vẫn có tight-coupling giữa các class.

### Cách code LV3

```java
public interface SortArg {
    void sort(int[] array);
}

public class BubbleSortAlg implements SortArg {
    @Override
    public void sort(int[] array) {
        // code bubble sort here
        System.out.println("Đã sắp xếp thep thuật toán nổi bọt");
    }
}

public class QuickSortAlg implements SortArg {
    @Override
    public void sort(int[] array) {
        // code bubble sort here
        System.out.println("Đã sắp xếp thep thuật toán quicksort");
    }
}

public class VeryComplexService {
    private SortArg sortArg;

    public VeryComplexService(SortArg sortArg) {
        this.sortArg = sortArg;
    }

    public void complexBusiness(int[] array) {
        this.sortArg.sort(array);
    }
}

public static void main(String[] args) {
    SortAlgorithm bubbleSortAlgorithm = new BubbleSortAlgorithm();
    SortAlgorithm quickSortAlgorithm = new QuicksortAlgorithm();
    VeryComplexService business1 = new VeryComplexService(bubbleSortAlgorithm);
    VeryComplexService business2 = new VeryComplexService(quickSortAlgorithm);
}
```

Với cách implement code như thế này, `VeryComplexService` chỉ tập trung vào nghiệp vụ của mình,
nó không quan tâm tới thuật toán sort là gì nữa. => Tuỳ từng trường hợp developer sử dụng thuật toán sort phù hợp.

## DI (Dependence Injection)

2 khái niệm `tight-coupling` và `loosely-coupled` là nền tảng cho khái niệm `Dependence Injection` - 
Một trong những khái niệm được sử dụng để build rất nhiều framework sử dụng strong type language. 

[NEXT - Dependency Injection vs IoC](02.DependenceInjection_and_IoC.md)